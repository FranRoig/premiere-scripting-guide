============================================
Welcome to the Premiere Pro Scripting Guide!
============================================

.. toctree::
    :maxdepth: 2
    :caption: Introduction

    1 - Introduction/index
    1 - Introduction/extendscript-overview
    1 - Introduction/how-to-execute-scripts

.. toctree::
    :maxdepth: 2
    :caption: App object

    2 - App object/application

.. toctree::
    :maxdepth: 2
    :caption: Project object

    3 - Project object/project

.. toctree::
    :maxdepth: 2
    :caption: Project Item object

    4 - Project Item object/projectItem

.. toctree::
    :maxdepth: 2
    :caption: Sequence object

    5 - Sequence object/sequence

.. toctree::
    :maxdepth: 2
    :caption: Track object

    6 - Track object/track

.. toctree::
    :maxdepth: 2
    :caption: Track Item object

    7 - Track Item object/trackitem

.. toctree::
    :maxdepth: 2
    :caption: Component object

    8 - Component object/component

.. toctree::
    :maxdepth: 2
    :caption: Component Parameter object

    9 - Component Parameter object/componentparam

.. toctree::
    :maxdepth: 2
    :caption: Anywhere object

    10 - Anywhere object/anywhere

.. toctree::
    :maxdepth: 2
    :caption: Encoder object

    11 - Encoder object/encoder

.. toctree::
    :maxdepth: 2
    :caption: Marker object

    12 - Marker object/marker

.. toctree::
    :maxdepth: 2
    :caption: Source object

    13 - Source object/source

.. toctree::
    :maxdepth: 2
    :caption: ProjectManager object

    14 - ProjectManager object/projectmanager

.. toctree::
    :maxdepth: 2
    :caption: AudioChannelMapping object

    15 - AudioChannelMapping object/audiochannelmapping

.. toctree::
    :maxdepth: 2
    :caption: Productions object

    16 - Productions object/productions
